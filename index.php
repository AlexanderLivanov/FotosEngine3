<!-- 10.11.2023 (c) Alexander Livanov -->

<?php
require_once('system/configs/dbcfg.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="system/static/content/css/style.css">
    <title>FotosEngine</title>
</head>

<body>
    <div class="feed">
        <div class="feed-item">
            <div class="card">

            </div>
            <div class="card">

            </div>
        </div>
        <div class="feed-item">
            <div class="card">

            </div>
            <div class="card">

            </div>
        </div>
        <div class="feed-item">
            <div class="card">

            </div>
            <div class="card">

            </div>
        </div>
    </div>
</body>

</html>